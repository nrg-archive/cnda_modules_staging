#!/bin/bash

BUILDPATH="."
REPO_URL="https://bitbucket.org"


if [ -s ./module_manifest.txt ]; then
    while read source_repo
    do
        repo=`awk -F/ '{print $2}' <<< ${source_repo}`
        repoOwn=`awk -F/ '{print $1}' <<< ${source_repo}`
        echo "Starting refresh for $repoOwn/$repo"

        bbJson=`curl -s "https://bitbucket.org/api/2.0/repositories/${repoOwn}/${repo}/commit/tip"`
        bbHash=`python -c "bbJson=${bbJson}; print bbJson['hash'][:12]"`
        echo "Hash of BitBucket tip: $bbHash"

        zipHash=`echo ${repoOwn}-${repo}-*.zip | sed 's/'${repoOwn}'-'${repo}'-\([^.]*\).zip/\1/'`
        echo "Hash in local zip: $zipHash"

        if [[ "$zipHash" == "$bbHash" && -n $zipHash ]]; then
            echo "Hashes are the same. Nothing to do."
        else
            echo "Hashes are different. Refreshing."
            rm ${repoOwn}-${repo}-*.zip
            dateTime=`date +%Y%m%d_%H%M`
            curl -k ${REPO_URL}/${repoOwn}/${repo}/get/tip.zip > ${BUILDPATH}/${repo}_${dateTime}.zip
            if [ ! -s ${BUILDPATH}/${repo}_${dateTime}.zip ]; then
                echo Cannot find ${BUILDPATH}/${repo}_${dateTime}.zip
                exit 1;
            fi
            cd ${BUILDPATH}
            unzip ${BUILDPATH}/${repo}_${dateTime}.zip
            zipDir=`ls | grep "${repoOwn}-${repo}-"`
            cd ${zipDir}
            zip -r ../${zipDir}.zip *
            cd ..
            rm -rf ${zipDir}
            rm ${repo}_${dateTime}.zip
        fi
    done < ./module_manifest.txt
else
    echo "Could not find module_manifest.txt."
fi
